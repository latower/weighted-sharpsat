#!/bin/bash

mkdir build
cd build

for build_type in mpfr mpq;
do
    mkdir ${build_type}
    cd ${build_type}
    build_type_upper=$(echo ${build_type} | awk '{print toupper($0)}')
    cmake -DCMAKE_BUILD_TYPE=${build_type_upper} ../..
    cd -
done
