
#ifndef MPX_H_
#define MPX_H_

#include <string>
#include <fstream>
#include <iostream>

using std::string;
using std::ifstream;
using std::cout;
using std::endl;

#ifdef MPQ
#include <gmpxx.h>
typedef mpq_class mpx_class;

inline double get_double(const mpq_class &n)
{
    return n.get_d();
}

inline string get_string(const mpq_class &n)
{
    return n.get_str();
}

inline mpq_class strtompq(const string& s){
	mpz_class num = 0, den = 1;
	unsigned int den_exp = 0;
	bool sign = true;
	bool in_frac = false;

	auto itr = s.cbegin();
	if (*itr == '-') {
		sign = false;
		itr++;
	}
	if (*itr == '+')
		itr++;
	while (*itr == '0')
		itr++;
	for(; itr!=s.cend(); itr++) {
		if (!isdigit(*itr)) { 
			if (*itr == '.') {
				in_frac = true;
				continue;
			}
			else {
				cout << "decimal number expected. input: " << s << endl;
				exit(1);
			}
        }
        
		if(in_frac)
			den_exp++;

		num = num * 10 + (*itr - '0');
	}
	mpz_ui_pow_ui(den.get_mpz_t(), 10, den_exp);

	mpq_class fraction(num, den);
	if (!sign)
		fraction = -fraction;
	return fraction;
}

inline mpq_class read_weight(ifstream &infile){
    string s;
    infile >> s;
    return strtompq(s);    
}

#define get_mc_size(X) ((X.get_num_mpz_t()->_mp_alloc + X.get_den_mpz_t()->_mp_alloc) * sizeof(mp_limb_t))
#define init_mc_size()
#endif

#ifdef MPFR
#include <mpreal.h>
using mpfr::mpreal;
typedef mpreal mpx_class;

extern unsigned MPFR_MC_SIZE; 

inline void init_mc_size(void){
    MPFR_MC_SIZE = ceil(mpreal::get_default_prec() / (double)mp_bits_per_limb) * sizeof(mp_limb_t);
}

inline void set_mpreal_ndigits(int ndigits){
    mpreal::set_default_prec(mpfr::digits2bits(ndigits));
    init_mc_size();
}

inline double get_double(const mpreal &n)
{
    return n.toDouble();
}

inline string get_string(const mpreal &n)
{
    return n.toString();
}

inline mpreal read_weight(ifstream &infile) {
    mpreal w;
    infile >> w;
    return w;
}

#define get_mc_size(X) MPFR_MC_SIZE
#endif

#endif