Weighted sharpSAT
==========

This repository contains a modified version of sharpSAT that supports 
weighted model counting instead of unweighted model counting. 

Specifically: this weighted sharpSAT code supports *literal weights* 
(and not *clause weights*) that represent probabilities. 
With each literal a weight *0 < w < 1* can be associated, where we 
assume that the negation of the literal has weight *1 - w*.

This weighted sharpSAT is built on Marc Thurley's sharpSAT that can be 
found [here](https://github.com/marcthurley/sharpSAT) (specifically, we made
modifications to the code as it is in [this commit](https://github.com/marcthurley/sharpSAT/commit/e57881a19edb3f896cfdd28cb42c0ecdc39ee1fe "sharpSAT commit e57881a19edb3f896cfdd28cb42c0ecdc39ee1fe")).

Input Format
--------------

Weighted sharpSAT takes as input a `.cnf` file of the following DIMACS-based format:

```
c Example input format for weighted sharpSAT
c
p cnf 3 4
w 1 0.2
w 2 0.75
1 -2 0
-1 2 -3 0
-3 2 0
1 3 0
```

The difference with standard DIMACS format is the lines starting with letter *w*. These lines specify the weights of literals. 

Here, variable/literal 1 has a weight of .2 (so literal -1 has a weight
of .8), and variable/literal 2 has a weight of .75 (so literal -2 has a
weight of .25). 

No weight is specified for variable/literal 3, which 
weighted sharpSAT interprets as literal 3 having a weight of 0.5 (and so has literal -3). 
In a sense this makes variable 3 unweighted, as the weight for literal 3 makes the same contribution to a weighted model count as the weight of literal -3.


Usage
------

```
Usage: sharpSAT [options] [CNF_File]
Options: 
         -noPP           turn off preprocessing
         -q              quiet mode
         -t [s]          set time bound to s seconds
         -noCC           turn off component caching
         -cs [n]         set max cache size to n MB
         -noIBCP         turn off implicit BCP
```

See above for instructions on input format.

Dependencies
--------------
You need the following pieces of software for building and running *weighted SharpSAT*:

- The [GCC](https://gcc.gnu.org/) compiler (version 4.7 or above)
- [CMake](https://cmake.org/) (version 2.8 or above)
- The [MPFR](https://www.mpfr.org/) library (version 3.1.6)
- The [MPG](https://gmplib.org/) library (version 6.1.2)
- The [MPFRC++](http://www.holoborodko.com/pavel/mpfr/) library (version 3.6.2)

Multiple-precision Arithmetic
----------------------------------
Weighted model counting requires arithmetic on numbers that are sometimes too small to be presented by floating-point numbers. For this reason we provide two versions of the code: one using rational numbers (mpq), and one using high-precision real numbers (mpfr). The build system allows you to choose between the two versions. 

The `mpq` version produces exact results, but the operations are more costly. We recommend using the `mpfr` version for better performance. 


Building
---------
First, run `./setupdev.sh` in the root directory of this repository. This will create a `build` directory with two subdirectories: `mpq` and `mpfr` (see section *Multiple-precision Arithmetic* above). 

The change the directory to `build/mpq` or `build/mpfr` and run `make`. This will generate the `sharpSAT` executable. 

Contributors
==========
The modifications needed to turn sharpSAT into a weighted model counter are made by Behrouz Babaki, Siegfried Nijssen and Anna Latour, who also maintain this repository.


Acknowledgments
==========
We are thankful to Marc Thurley for giving us permission to build on the sharpSAT code. We also thank [Medallia](https://www.medallia.com/) for helping us in the process of making our code publicly available.  